class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'access_ipg_filter': self.access_ipg_filter
        }
    
    def access_ipg_filter(self, aci_access_ipg_result):
        '''
        Filter for fabric access interface policy group infraAccPortGrp.
        '''
        structured_data_access_ipg = {'access_interface_policy_group': {}}
    
        for item in aci_access_ipg_result:
            attr = item['infraAccPortGrp']['attributes']
            item_dn = attr['dn']
            attr_children = item['infraAccPortGrp']['children']
            item_fc = next(child["infraRsFcIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsFcIfPol" in child)
            item_cdp = next(child["infraRsCdpIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsCdpIfPol" in child)
            item_lldp = next(child["infraRsLldpIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsLldpIfPol" in child)
            item_l2 = next(child["infraRsL2IfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsL2IfPol" in child)
            item_port_sec = next(child["infraRsL2PortSecurityPol"]["attributes"]["tDn"] for child in attr_children if "infraRsL2PortSecurityPol" in child)
            item_link_level_flow_control = next(child["infraRsQosPfcIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsQosPfcIfPol" in child)
            item_link_level = next(child["infraRsHIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsHIfPol" in child)
            item_monitor = next(child["infraRsMonIfInfraPol"]["attributes"]["tDn"] for child in attr_children if "infraRsMonIfInfraPol" in child)
            item_stp = next(child["infraRsStpIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsStpIfPol" in child)
            item_aeep = next(child["infraRsAttEntP"]["attributes"]["tDn"] for child in attr_children if "infraRsAttEntP" in child)
            item_mcp = next(child["infraRsMcpIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsMcpIfPol" in child)
            item_egress_date_plane = next(child["infraRsQosEgressDppIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsQosEgressDppIfPol" in child)
            item_storm_control = next(child["infraRsStormctrlIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsStormctrlIfPol" in child)
            item_ingress_data_plane = next(child["infraRsQosIngressDppIfPol"]["attributes"]["tDn"] for child in attr_children if "infraRsQosIngressDppIfPol" in child)

            if item_dn not in structured_data_access_ipg['access_interface_policy_group']:
                structured_data_access_ipg['access_interface_policy_group'][item_dn] = {}
                
            structured_data_access_ipg['access_interface_policy_group'][item_dn].update({
                'dn': item_dn,
                'fibre_channel_pol': item_fc,
                'cdp_pol': item_cdp,
                'lldp_pol': item_lldp,
                'l2_interface': item_l2,
                'port_security': item_port_sec,
                'link_level_flow_control': item_link_level_flow_control,
                'link_level': item_link_level,
                'monitoring': item_monitor,
                'spanning_tree': item_stp,
                'aaep': item_aeep,
                'mcp': item_mcp,
                'egress_date_plane': item_egress_date_plane,
                'storm_control': item_storm_control,
                'ingress_data_plane': item_ingress_data_plane
            })

        return structured_data_access_ipg