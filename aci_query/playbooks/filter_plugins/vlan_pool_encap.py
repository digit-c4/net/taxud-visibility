class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'vlan_pool_encap_filter': self.vlan_pool_encap_filter
        }
    
    def vlan_pool_encap_filter(self, aci_vl_pool_encap_result):
        structured_data = {'vlan_pool_encap': {}}

        for item in aci_vl_pool_encap_result:
            item_dn = item['fvnsEncapBlk']['attributes']['dn']
            item_allocation_mode = item['fvnsEncapBlk']['attributes']['allocMode']

            if item_dn not in structured_data['vlan_pool_encap']:
                structured_data['vlan_pool_encap'][item_dn] = {}

            structured_data['vlan_pool_encap'][item_dn].update({
                'dn': item_dn,
                'allocation_mode': item_allocation_mode
            })

        return structured_data

