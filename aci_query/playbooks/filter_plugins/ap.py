class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'ap_filter': self.ap_filter,
        }
    
    def ap_filter(self, aci_ap_result, tenant_netbox):
        '''
        Filter for fabric all APs fvAp.
        Filters out data not matching the specified tenant from NetBox.
        '''

        # Define an empty dictionary which will represent the BDs with the parameters
        structured_data = {'aps': []}

        for item in aci_ap_result:
            item_dn = item['fvAp']['attributes']['dn']
            item_tenant = item['fvAp']['attributes']['dn'].split('/')[1].replace('tn-','')
            
            if item_tenant in tenant_netbox:
                structured_data['aps'].append(item_dn)

        return structured_data