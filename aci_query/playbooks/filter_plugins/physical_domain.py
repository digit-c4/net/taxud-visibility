class FilterModule(object):
    ''' Extend the filter to include EPG relationships like fvRsBd '''

    def filters(self):
        return {
            'physical_domain_filter': self.physical_domain_filter
        }
    
    def physical_domain_filter(self, aci_vl_pool_phys_domain_result):
        structured_data = {'phys_domains': {}}

        for result in aci_vl_pool_phys_domain_result['results']:
            for item in result['current']:
                attr = item['physDomP']['attributes']
                item_domain = attr['dn']
                attr_children = item['physDomP'].get('children', {'No child objects for this domain.'})
                item_vlan_pool = next((child['infraRsVlanNs']['attributes']['tDn'] for child in attr_children if 'infraRsVlanNs' in child), None)

                if item_domain not in structured_data['phys_domains']:
                    structured_data['phys_domains'][item_domain] = {}

                structured_data['phys_domains'][item_domain].update({
                    'vlan_pool': item_vlan_pool if item_vlan_pool else "Not found",
                })

        return structured_data

