class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'bd_l2_filter': self.bd_l2_filter,
        }
    
    def bd_l2_filter(self, aci_l2_bd_result, tenant_netbox):
        '''
        Filter for fabric all BDs fvBD and VRFs fvRsCtx.
        Filters out data not matching the specified tenant from NetBox.
        '''

        # Define an empty dictionary which will represent the BDs with the parameters
        structured_data = {'bds': {}}

        for item in aci_l2_bd_result:
            item_dn = item['fvBD']['attributes']['dn']
            item_tenant = item['fvBD']['attributes']['dn'].split('/')[1].replace('tn-','')
            item_vrf = next(child['fvRsCtx']['attributes']['tnFvCtxName'] for child in item['fvBD']['children'] if 'fvRsCtx' in child)
            
            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in structured_data['bds']:
                structured_data['bds'][item_dn] = {}

            structured_data['bds'][item_dn].update({
                'vrf': item_vrf
            })

        return structured_data