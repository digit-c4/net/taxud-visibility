class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'physical_interface_filter': self.physical_interface_filter,
            'physical_interface_details_filter': self.physical_interface_details_filter,
            'physical_interface_tranciever_filter': self.physical_interface_tranciever_filter,
            'interface_events_filter': self.interface_events_filter,
            'interface_faults_filter': self.interface_faults_filter
        }
    
    def physical_interface_filter(self, aci_physical_interface_result):
        '''
        Filter for fabric all physical interface l1PhysIf.
        '''
        structured_data_phys_intf = {'phys_interf': {}}
    
        for item in aci_physical_interface_result:
            item_dn = item['l1PhysIf']['attributes']['dn']
            item_autoneg = item['l1PhysIf']['attributes']['autoNeg']
            item_descr = item['l1PhysIf']['attributes']['descr']
            item_layer = item['l1PhysIf']['attributes']['layer']
            item_mode = item['l1PhysIf']['attributes']['mode']
            item_mtu = item['l1PhysIf']['attributes']['mtu']
            item_speed = item['l1PhysIf']['attributes']['speed']
            item_usage = item['l1PhysIf']['attributes']['usage']

            if item_usage != 'fabric':
                structured_data_phys_intf['phys_interf'][item_dn] = {
                    'description': item_descr,
                    'auto_negotiation': item_autoneg,
                    'layer': item_layer,
                    'mode': item_mode,
                    'mtu': item_mtu,
                    'speed': item_speed
                }

        return structured_data_phys_intf
    
    def physical_interface_details_filter(self, aci_physical_interface_details_result):
        '''
        Filter for fabric all physical interface ethpmPhysIf.
        '''
        structured_data_phys_intf = {'phys_interf_detail': {}}
    
        for item in aci_physical_interface_details_result:
            item_dn = item['ethpmPhysIf']['attributes']['dn']
            item_po = item['ethpmPhysIf']['attributes']['bundleIndex']
            item_usage = item['ethpmPhysIf']['attributes']['usage']
            item_mode = item['ethpmPhysIf']['attributes']['operMode']
            item_speed = item['ethpmPhysIf']['attributes']['operSpeed']
            item_oper_state = item['ethpmPhysIf']['attributes']['operSt']
            item_oper_state_reason = item['ethpmPhysIf']['attributes']['operStQual']

            if item_usage != 'fabric':
                structured_data_phys_intf['phys_interf_detail'][item_dn] = {
                    'port_channel': item_po,
                    'mode': item_mode,
                    'speed': item_speed,
                    'state': item_oper_state,
                    'state_reason': item_oper_state_reason
                }
    
        return structured_data_phys_intf
    
    def physical_interface_tranciever_filter(self, aci_physical_interface_tranciever_result):
        '''
        Filter for fabric all interface trancievers ethpmFcot.
        '''
        structured_data_tranciever = {'interf_tranciever': {}}
    
        for item in aci_physical_interface_tranciever_result:
            attr = item['ethpmFcot']['attributes']
            item_dn = attr['dn']
            item_pid = attr['guiCiscoPID']
            item_sn = attr['guiSN']
            item_name = attr['guiName']

            structured_data_tranciever['interf_tranciever'][item_dn] = {
                'pid': item_pid.strip(),
                'sn': item_sn,
                'item_name': item_name
            }

        return structured_data_tranciever

    def interface_faults_filter(self, aci_fabric_interface_faults_result):
        '''
        Filter to extract the faults from the interfaces eth/po/vPC.
        '''
        pass

        structured_data_intf_fault = {'interface_faults': {}}

        for item in aci_fabric_interface_faults_result:
            try:
                attr = item['faultRecord']['attributes']
                item_affected = attr.get('affected')
                item_cause = attr.get('cause')
                item_change_set = attr.get('changeSet')
                item_code = attr.get('code')
                item_created = attr.get('created')
                item_desc = attr.get('descr')
                item_dn = attr.get('dn')
                item_id = attr.get('id')
                item_action = attr.get('ind')
                item_rule = attr.get('rule')
                item_severity = attr.get('severity')
                item_life_cycle = attr.get('lc')

                if item_dn not in structured_data_intf_fault['interface_faults']:
                    structured_data_intf_fault['interface_faults'][item_dn] = {}

                structured_data_intf_fault['interface_faults'][item_dn].update({
                    'dn': item_dn,
                    'object_affected': item_affected,
                    'cause':  item_cause,
                    'change_set': item_change_set,
                    'code': item_code,
                    'data_created': item_created,
                    'description': item_desc,
                    'event_id': item_id,
                    'action': item_action,
                    'rule': item_rule,
                    'severity': item_severity,
                    'life_cycle': item_life_cycle
                })

            except Exception as e:
                print(f'Error in the filter plugin for interface fault: {e}')
    
        return structured_data_intf_fault
            
    
    def interface_events_filter(self, aci_fabric_interface_events_result):
        '''
        Filter to extract the events from the interfaces eth/po/vPC.
        '''

        structured_data_intf_event = {'interface_events': {}}

        for item in aci_fabric_interface_events_result:
            try:
                attr = item['eventRecord']['attributes']
                item_id = attr.get('id')
                item_affected = attr.get('affected')
                item_cause = attr.get('cause')
                item_change_set = attr.get('changeSet')
                item_code = attr.get('code')
                item_created = attr.get('created')
                item_descr = attr.get('descr')
                item_dn = attr.get('dn')
                item_action = attr.get('ind')
                item_severity = attr.get('severity')

                if item_dn not in structured_data_intf_event['interface_events']:
                    structured_data_intf_event['interface_events'][item_dn] = {}

                structured_data_intf_event['interface_events'][item_dn].update({
                    'event_id': item_id,
                    'object_affected': item_affected,
                    'cause': item_cause,
                    'change_set': item_change_set,
                    'event_code': item_code,
                    'date_created': item_created,
                    'description': item_descr,
                    'dn': item_dn,
                    'action': item_action,
                    'severity': item_severity
                    })

            except Exception as e:
                print(f'Error in the filter plugin for interface event: {e}')
    
        return structured_data_intf_event