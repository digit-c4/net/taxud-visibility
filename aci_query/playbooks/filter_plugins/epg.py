class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'epg_filter': self.epg_filter,
            'epg_domain_filter': self.epg_domain_filter,
            'epg_binding_filter': self.epg_binding_filter,
            'epg_contract_consumer_filter': self.epg_contract_consumer_filter,
            'epg_contract_provider_filter': self.epg_contract_provider_filter,
            'epg_mac_filter': self.epg_mac_filter
        }
    
    def epg_filter(self, aci_epgs, tenant_netbox):
        '''
        Filter for fabric all EPGs fvAEPg.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'epgs': {}}

        for item in aci_epgs:
            item_dn = item['fvAEPg']['attributes']['dn']
            item_tenant = item_dn.split('/')[1].replace('tn-', '')
            item_descr = item['fvAEPg']['attributes']['descr']
            item_flood_on_encap = item['fvAEPg']['attributes']['floodOnEncap']
            item_enforce_mode = item['fvAEPg']['attributes']['pcEnfPref']
            item_pc_tag = item['fvAEPg']['attributes']['pcTag']
            item_prefered_group = item['fvAEPg']['attributes']['prefGrMemb']
            item_assigned_bd = next(child['fvRsBd']['attributes']['tDn'] for child in item['fvAEPg']['children'] if 'fvRsBd' in child )

            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in structured_data['epgs']:
                structured_data['epgs'][item_dn] = {}

            structured_data['epgs'][item_dn].update({
                'description': item_descr,
                'flood_in_encap': item_flood_on_encap,
                'intra_isolation': item_enforce_mode,
                'pctag': item_pc_tag,
                'preferred_group': item_prefered_group,
                'bd': item_assigned_bd
            })

        return structured_data
    
    def epg_domain_filter(self, aci_epg_domain, tenant_netbox):
        '''
        Filter for fabric all EPGs with domain fvRsDomAtt.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'epg_domains': []}

        for item in aci_epg_domain:
            item_dn = item['fvRsDomAtt']['attributes']['dn']
            item_tenant = item['fvRsDomAtt']['attributes']['dn'].split('/')[1].replace('tn-','')
            
            if item_tenant not in tenant_netbox:
                continue

            structured_data['epg_domains'].append(item_dn)

        return structured_data
    
    def epg_binding_filter(self, aci_epg_binding_result, tenant_netbox):
        '''
        Filter for fabric all EPGs with bindings fvRsPathAtt.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'epg_bindings': {}}

        for item in aci_epg_binding_result:
            item_dn = item['fvRsPathAtt']['attributes']['dn']
            item_tenant = item['fvRsPathAtt']['attributes']['dn'].split('/')[1].replace('tn-','')
            item_encap = item['fvRsPathAtt']['attributes']['encap']
            item_primary_encap = item['fvRsPathAtt']['attributes']['primaryEncap']
            item_mode = item['fvRsPathAtt']['attributes']['mode']
            item_deployment_immediacy = item['fvRsPathAtt']['attributes']['instrImedcy']
            
            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in  structured_data['epg_bindings']:
                structured_data['epg_bindings'][item_dn] = {}

            structured_data['epg_bindings'][item_dn].update({
                'encapsulation': item_encap,
                'primary_encapsulation': item_primary_encap,
                'mode': item_mode,
                'deployment_immediacy': item_deployment_immediacy
            })

        return structured_data
    
    def epg_contract_consumer_filter(self, aci_epg_domain, tenant_netbox):
        '''
        Filter for fabric all EPGs with consumer contracts fvRsCons.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'epg_consumer_contracts': []}

        for item in aci_epg_domain:
            item_dn = item['fvRsCons']['attributes']['dn']
            item_tenant = item['fvRsCons']['attributes']['dn'].split('/')[1].replace('tn-','')
            
            if item_tenant not in tenant_netbox:
                continue

            structured_data['epg_consumer_contracts'].append(item_dn)

        return structured_data
    
    def epg_contract_provider_filter(self, aci_epg_domain, tenant_netbox):
        '''
        Filter for fabric all EPGs with provider contracts fvRsProv.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'epg_provider_contracts': []}

        for item in aci_epg_domain:
            item_dn = item['fvRsProv']['attributes']['dn']
            item_tenant = item['fvRsProv']['attributes']['dn'].split('/')[1].replace('tn-','')
            
            if item_tenant not in tenant_netbox:
                continue 

            structured_data['epg_provider_contracts'].append(item_dn)

        return structured_data
    
    def epg_mac_filter(self, aci_epg_mac_result, tenant_netbox):
        '''
        Filter for fabric all EPGs with bindings fvCEp.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'epg_macs': {}}

        for item in aci_epg_mac_result:
            item_dn = item['fvCEp']['attributes']['dn']
            item_tenant = item['fvCEp']['attributes']['dn'].split('/')[1].replace('tn-','')
            item_path = item['fvCEp']['attributes']['fabricPathDn']
            item_encap = item['fvCEp']['attributes']['encap']
            item_bd = item['fvCEp']['attributes']['bdDn']
            
            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in structured_data['epg_macs']:
                structured_data['epg_macs'][item_dn] = {}

            structured_data['epg_macs'][item_dn].update({
                'path': item_path,
                'encapsulation': item_encap,
                'bd': item_bd
            })

        return structured_data