class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'contract_filter': self.contract_filter,
            'contract_subject_filter': self.contract_subject_filter,
            'contract_subject_entry_filter': self.contract_subject_entry_filter
        }
    
    def contract_filter(self, aci_contract_result, tenant_netbox):
        '''
        Filter for fabric all Contracts vzBrCP.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'contracts': {}}

        for item in aci_contract_result:
            item_dn = item['vzBrCP']['attributes']['dn']
            item_tenant = item_dn.split('/')[1].replace('tn-', '')
            item_scope = item['vzBrCP']['attributes']['scope']

            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in structured_data['contracts']:
                structured_data['contracts'][item_dn] = {}

            structured_data['contracts'][item_dn].update({
                'scope': item_scope
            })

        return structured_data
    
    def contract_subject_filter(self, aci_contract_subject_result, tenant_netbox):
        '''
        Filter for fabric all Contract with subjects vzSubj.
        Filters out data not matching the specified tenant from NetBox.
        '''

        structured_data = {'contract_subjects': {}}

        for item in aci_contract_subject_result:
            item_dn = item['vzSubj']['attributes']['dn']
            item_tenant = item['vzSubj']['attributes']['dn'].split('/')[1].replace('tn-','')
            item_revert_filter_port = item['vzSubj']['attributes']['revFltPorts']

            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in structured_data['contract_subjects']:
                structured_data['contract_subjects'][item_dn] = {}

            structured_data['contract_subjects'][item_dn].update({
                'reverse_filter_port': item_revert_filter_port
            })

        return structured_data

    def contract_subject_entry_filter(self, aci_contract_filter_result, tenant_netbox):
        '''
        Filter for fabric all Contract with subjects and entries/filters vzRsSubjFiltAtt.
        Filters out data not matching the specified tenant from NetBox.
        '''
        
        structured_data = {'contract_subject_filters': {}}

        for item in aci_contract_filter_result:
            item_dn = item['vzRsSubjFiltAtt']['attributes']['dn']
            item_tenant = item['vzRsSubjFiltAtt']['attributes']['dn'].split('/')[1].replace('tn-','')
            item_action = item['vzRsSubjFiltAtt']['attributes']['action']
            
            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in structured_data['contract_subject_filters']:
                structured_data['contract_subject_filters'][item_dn] = {}

            structured_data['contract_subject_filters'][item_dn].update({
                'action': item_action
            })

        return structured_data