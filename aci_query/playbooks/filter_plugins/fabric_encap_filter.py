class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'fabric_encap_filter': self.fabric_encap_filter
        }
    
    def fabric_encap_filter(self, aci_fabric_encap_result):
        '''
        Filter for fabric all vlans encap vlanCktEp.
        Filters out data not matching the specified tenant from NetBox.
        '''
        fabric_encap_data = {'fabric_vlans_vlanCktEp': []}
        seen_encap_epg = set() # Track seen (encap, epg) pairs for filtering duplicates

        for item in aci_fabric_encap_result:
            try:
                item_encap = item['vlanCktEp']['attributes']['encap']
                encap_epg_pair = (item_encap) # Create a tuple of encap and epgDn for uniqueness checking
                
                if encap_epg_pair not in seen_encap_epg:
                    seen_encap_epg.add(encap_epg_pair)
                    fabric_encap_data['fabric_vlans_vlanCktEp'].append(item_encap)

            except Exception as e:
                print(f'Something went wrong with the fabric vlan attributes filter: {e}')

        return fabric_encap_data