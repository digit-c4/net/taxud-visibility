import re

class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'vlan_deploy_filter': self.vlan_deploy_filter
        }
    
    def vlan_deploy_filter(self, aci_vldeploy_result, tenant_netbox):
        '''
        Filter for fabric VLAN deployment encap fvIfConn.
        '''
        unique_encap_set = set()
        structured_data_vldploy = []

        tenant_pattern = r"uni/tn-(.*?)/"
        l3out_pattern = r"/out-"

        for item in aci_vldeploy_result:
            attr = item['fvIfConn']['attributes']
            item_dn = attr['dn']

            if re.search(l3out_pattern, item_dn):
                continue

            tenant_match = re.search(tenant_pattern, item_dn)
            if tenant_match:
                item_tenant = tenant_match.group(1)
            else:
                continue

            if item_tenant not in tenant_netbox:
                continue

            if item_dn not in unique_encap_set:
                structured_data_vldploy.append(item_dn)
                unique_encap_set.add(item_dn)

        return structured_data_vldploy