class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'port_channel_filter': self.port_channel_filter,
            'port_channel_details_filter': self.port_channel_details_filter
        }
    
    def port_channel_filter(self, aci_po_result):
        '''
        Filter for fabric all port-channels pcAggrIf.
        '''
        structured_data_po = {'port_channel': {}}
    
        for item in aci_po_result:
            attr = item['pcAggrIf']['attributes']
            item_dn = attr['dn']
            item_active_ports = attr['activePorts']
            item_admin_state = attr['adminSt']
            item_auto_negociation = attr['autoNeg']
            item_port1 = attr['fop']
            item_port2 = attr['lastBundleMbr']
            item_layer = attr['layer']
            item_name = attr['name']
            item_mtu = attr['mtu']
            item_mode = attr['mode']
            item_protocol = attr['pcMode']
            item_speed = attr['speed']

            if item_dn not in structured_data_po['port_channel']:
                structured_data_po['port_channel'][item_dn] = {}

            structured_data_po['port_channel'][item_dn].update({
                'active_members': item_active_ports,
                'admin_state': item_admin_state,
                'autonegotiation': item_auto_negociation,
                'member_port1': item_port1,
                'member_port2': item_port2,
                'interface_policy_group': item_name,
                'mtu': item_mtu,
                'mode': item_mode,
                'layer': item_layer,
                'lacp_protocol': item_protocol,
                'speed': item_speed
            })

        return structured_data_po
    
    def port_channel_details_filter(self, aci_po_details_result):
        '''
        Filter for fabric all port channel details ethpmAggrIf.
        '''
        structured_data_po = {'port_channel_details': {}}
    
        for item in aci_po_details_result:
            attr = item['ethpmAggrIf']['attributes']
            item_dn = attr['dn']
            item_active_ports = attr['activeMbrs']
            item_state = attr['operSt']
            item_state_reason = attr['operStQual']
            item_mode = attr['operMode']

            if item_dn not in structured_data_po['port_channel_details']:
                structured_data_po['port_channel_details'][item_dn] = {}

            structured_data_po['port_channel_details'][item_dn].update({
                'active_members': item_active_ports.replace('unspecified', '').replace(',',''),
                'state': item_state,
                'state_reason': item_state_reason,
                'mode': item_mode
            })

        return structured_data_po