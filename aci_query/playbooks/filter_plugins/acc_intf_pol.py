class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'acc_intf_pol_filter': self.acc_intf_pol_filter
        }
    
    def acc_intf_pol_filter(self, aci_acc_intf_pol_result):
        '''
        Filter for fabric access interface policies.
        '''
        structured_data = []
    
        for item in aci_acc_intf_pol_result:
            for key, value in item.items():
                if 'cdpIfPol' in key:
                    item_cdp = value['attributes']['dn']
                    item_cdp_state = value['attributes']['adminSt']
                    structured_data.append({'cdp':{'cdp_pol': item_cdp, 'state': item_cdp_state }})
                elif 'coppIfPol' in key:
                    item_copp = value['attributes']['dn']
                    structured_data.append({'copp':{'copp_pol': item_copp}})
                elif 'qosDppPol' in key:
                    item_data_plane_policing = value['attributes']['dn']
                    item_data_plane_policing_state = value['attributes']['adminSt']
                    structured_data.append({'data_plane_policing':{'data_plane_policing_pol': item_data_plane_policing, 'state': item_data_plane_policing_state}})
                elif 'dwdmIfPol' in key:
                    item_dwdm = value['attributes']['dn']
                    structured_data.append({'dwdm':{'dwdm_pol': item_dwdm}})
                elif 'l2IfPol' in key:
                    item_l2_interface = value['attributes']['dn']
                    item_l2_interface_qinq = value['attributes']['qinq']
                    item_l2_interface_qbg = value['attributes']['vepa']
                    item_l2_interface_vl_scope =  value['attributes']['vlanScope']
                    structured_data.append({'l2_interface':{'l2_interface_pol': item_l2_interface,
                                                            'qinq': item_l2_interface_qinq,
                                                            'qbg': item_l2_interface_qbg,
                                                            'vlan_scope': item_l2_interface_vl_scope}})
                elif 'fabricLinkFlapPol' in key:
                    item_link_flap = value['attributes']['dn']
                    item_lf_max_flap = value['attributes']['linkFlapErrorMax']
                    item_lf_err_disable = value['attributes']['linkFlapErrorSeconds']
                    structured_data.append({'link_flap':{'link_flap_pol': item_link_flap, 
                                                         'max_flaps':item_lf_max_flap,
                                                          'duration_max_flaps': item_lf_err_disable}})
                elif 'fabricHIfPol' in key:
                    item_link_level = value['attributes']['dn']
                    item_link_level_auto_nego = value['attributes']['autoNeg']
                    item_link_level_desc = value['attributes']['descr']
                    item_link_level_link_debounce = value['attributes']['linkDebounce']
                    item_link_level_fec = value['attributes']['fecMode']
                    item_link_media_type = value['attributes']['portPhyMediaType']
                    item_link_level_speed = value['attributes']['speed']
                    item_link_level_emi = value['attributes']['emiRetrain']
                    structured_data.append({'link_level':{  'link_level_pol': item_link_level,
                                                            'auto_negotiation': item_link_level_auto_nego,
                                                            'description': item_link_level_desc,
                                                            'fec': item_link_level_fec,
                                                            'physical_media_type': item_link_media_type,
                                                            'speed': item_link_level_speed,
                                                            'emi_retrain': item_link_level_emi,
                                                            'link_debounce_timer': item_link_level_link_debounce}})
                elif 'lldpIfPol' in key:
                    item_lldp = value['attributes']['dn']
                    item_lld_rx_state = value['attributes']['adminRxSt']
                    item_lld_tx_state = value['attributes']['adminTxSt']
                    structured_data.append({'lldp':{'lldp_pol': item_lldp, 'rx_state': item_lld_rx_state, 'tx_state': item_lld_tx_state}})
                elif 'mcpIfPol' in key:
                    item_mcp = value['attributes']['dn']
                    item_mpc_state = value['attributes']['adminSt']
                    item_mcp_mode = value['attributes']['mcpMode']
                    structured_data.append({'mcp':{'mcp_pol': item_mcp, 'state': item_mpc_state, 'mode': item_mcp_mode}})
                elif 'lacpLagPol' in key:
                    item_po = value['attributes']['dn']
                    item_po_ctrl = value['attributes']['ctrl']
                    item_po_descr = value['attributes']['descr']
                    item_po_mode = value['attributes']['mode']
                    item_po_min_links = value['attributes']['minLinks']
                    item_po_max_links = value['attributes']['maxLinks']
                    
                    structured_data.append({'port_channel':{'port_channel_pol': item_po, 'control': item_po_ctrl, 'description': item_po_descr,
                                                            'lacp_mode': item_po_mode, 'min_links': item_po_min_links, 'maxim_links': item_po_max_links}})
                elif 'lacpIfPol' in key:
                    item_po = value['attributes']['dn']
                    item_po_rate = value['attributes']['txRate']
                    item_po_prio = value['attributes']['prio']
                    structured_data.append({'lacp_rate':{'lacp_pol': item_po, 'rate': item_po_rate, 'priority': item_po_prio }})
                elif 'l2PortSecurityPol' in key:
                    item_port_security = value['attributes']['dn']
                    item_port_security_timeout = value['attributes']['timeout']
                    item_port_security_max_ep = value['attributes']['maximum']
                    item_port_security_violation = value['attributes']['violation']
                    structured_data.append({'port_security':{'port_security_pol': item_port_security, 'timeout': item_port_security_timeout, 'maximum_endpoints': item_port_security_max_ep, 'violation': item_port_security_violation }})
                elif 'stpIfPol' in key:
                    item_stp = value['attributes']['dn']
                    item_stp_ctrl = value['attributes']['ctrl']
                    structured_data.append({'spanning_tree':{'stp_pol': item_stp, 'iterface_controls': item_stp_ctrl}})
                elif 'stormctrlIfPol' in key:
                    item_storm_control = value['attributes']['dn']

                    item_sc_bc_burst_pps = value['attributes']['bcBurstPps']
                    item_sc_bc_max_burst_rate = value['attributes']['bcBurstRate']
                    item_sc_bc_bw_percentage = value['attributes']['bcRate']
                    item_sc_bc_rate_pps = value['attributes']['bcRatePps']
                    item_sc_burst_pps = value['attributes']['burstPps']
                    item_sc_burst_rate = value['attributes']['burstRate']

                    item_sc_mc_burst_pps = value['attributes']['mcBurstPps']
                    item_sc_mc_max_burst_rate = value['attributes']['mcBurstRate']
                    item_sc_mc_bw_percentage = value['attributes']['mcRate']
                    item_sc_mc_rate_pps = value['attributes']['mcRatePps']
                    
                    item_sc_uu_burst_pps = value['attributes']['mcBurstPps']
                    item_sc_uu_max_burst_rate = value['attributes']['uucBurstRate']
                    item_sc_uu_bw_percentage = value['attributes']['uucRate']
                    item_sc_uu_rate_pps = value['attributes']['uucRatePps']

                    item_sc_action = value['attributes']['stormCtrlAction']

                    structured_data.append({'storm_control':
                                            {
                                                'sc_pol': item_storm_control,
                                                'broadcast_burst_pps':item_sc_bc_burst_pps ,'broadcast_max_burst_rate': item_sc_bc_max_burst_rate,
                                                'broadcast_bw_percentage': item_sc_bc_bw_percentage, 'broadcast_rate_pps':  item_sc_bc_rate_pps,
                                                'broadcast_burst_pps': item_sc_burst_pps, 'broadcast_burst_rate': item_sc_burst_rate,
                                                'multicast_burst_pps':item_sc_mc_burst_pps ,'multicats_max_burst_rate': item_sc_mc_max_burst_rate,
                                                'multicast_bw_percentage': item_sc_mc_bw_percentage, 'multicats_rate_pps':  item_sc_mc_rate_pps,
                                                'unicast_burst_pps':item_sc_uu_burst_pps ,'broadcast_max_burst_rate': item_sc_uu_max_burst_rate,
                                                'unicast_bw_percentage': item_sc_uu_bw_percentage, 'broadcast_rate_pps':  item_sc_uu_rate_pps,
                                                'storm_control_action': item_sc_action
                                            }})
        return structured_data