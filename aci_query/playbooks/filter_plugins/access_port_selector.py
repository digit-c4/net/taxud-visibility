class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'access_port_selector_filter': self.access_port_selector_filter,
        }
    
    def access_port_selector_filter(self, aci_access_selector_result):
        """
        Filter for fabric all access port selectors with IPG infraHPort & infraPortBlk.
        """

        structured_data = {'access_port_selector': {}}

        for item in aci_access_selector_result:
            item_lip_dn = item['infraHPortS']['attributes']['dn']
            item_lip_descr = item['infraHPortS']['attributes']['descr']

            item_aps_descr = None
            item_aps_fcard = None
            item_aps_fport = None
            item_aps_tcard = None
            item_aps_tport = None
            item_policy_group = None

            children = item['infraHPortS'].get('children', [])
            for child in children:
                if 'infraPortBlk' in child:
                    infra_port_blk = child['infraPortBlk']['attributes']
                    item_aps_descr = infra_port_blk.get('descr')
                    item_aps_fcard = infra_port_blk.get('fromCard')
                    item_aps_fport = infra_port_blk.get('fromPort')
                    item_aps_tcard = infra_port_blk.get('toCard')
                    item_aps_tport = infra_port_blk.get('toPort')
                elif 'infraRsAccBaseGrp' in child:
                    infra_rs_acc_base_grp = child['infraRsAccBaseGrp']['attributes']
                    item_policy_group = infra_rs_acc_base_grp.get('tDn')

            if item_lip_dn not in structured_data['access_port_selector']:
                structured_data['access_port_selector'][item_lip_dn] = []

            structured_data['access_port_selector'][item_lip_dn].append({
                'leaf_interface_profile_descr': item_lip_descr,
                'interface_description': item_aps_descr,
                'ports': f'{item_aps_fcard}/{item_aps_fport}-{item_aps_tcard}/{item_aps_tport}',
                'policy_group': item_policy_group
            })

        return structured_data
