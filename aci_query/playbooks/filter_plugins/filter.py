class FilterModule(object):
    ''' Class to define filters based on the cisco ACI interogation'''

    def filters(self):
        return {
            'flt_filter': self.flt_filter,
            'flt_entry_filter': self.flt_entry_filter
        }
    
    def flt_filter(self, aci_filter_result):
        '''
        Filter for fabric all filters vzFilter.
        '''

        structured_data = {'filters': []}

        for item in aci_filter_result:
            item_dn = item['vzFilter']['attributes']['dn']
            structured_data['filters'].append(item_dn)

        return structured_data
    
    def flt_entry_filter(self, aci_filter_entry_result):
        '''
        Filter for fabric all filters vzEntry.
        '''

        structured_data= {'filters_entry': {}}

        for item in aci_filter_entry_result:
            item_dn = item['vzEntry']['attributes']['dn']
            item_from_dport = item['vzEntry']['attributes']['dFromPort']
            item_to_dport = item['vzEntry']['attributes']['dToPort']
            item_ether_type = item['vzEntry']['attributes']['etherT']
            item_protocol = item['vzEntry']['attributes']['prot']
            item_from_sport = item['vzEntry']['attributes']['sFromPort']
            item_to_sport = item['vzEntry']['attributes']['sToPort']

            if item_dn not in structured_data['filters_entry']:
                structured_data['filters_entry'][item_dn] = {}

            structured_data['filters_entry'][item_dn].update({
                'from_dport': item_from_dport,
                'to_dport': item_to_dport,
                'ethertype': item_ether_type,
                'ip_protocol': item_protocol,
                'from_sport': item_from_sport,
                'to_sport': item_to_sport
            })

        return structured_data